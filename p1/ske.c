#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	if (entropy == NULL){
		randBytes(K->hmacKey,HM_LEN);
		randBytes(K->aesKey,HM_LEN);
	} else {
		memcpy(K->hmacKey, HMAC(EVP_sha512(), KDF_KEY, HM_LEN, entropy, entLen, NULL, NULL), HM_LEN);
		memcpy(K->aesKey, HMAC(EVP_sha512(), KDF_KEY, HM_LEN, entropy, entLen, NULL, NULL), HM_LEN);
		printf("khmackey %s\n", K->hmacKey);
		printf("haeskey  %s\n", K->aesKey);
	}
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	unsigned char* ct = malloc(ske_getOutputLen(len));
	unsigned char* hmac_ct = malloc(32);
	//AES on the plaintext
	if(IV == NULL || strlen((char*)IV) == 0){
		IV = malloc(16);
		randBytes(IV,16);
	}

	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	int nWritten;
	if (1!=EVP_EncryptUpdate(ctx,ct,&nWritten,inBuf,len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);
	size_t ctLen = nWritten;
	memcpy(outBuf, IV, 16);
	memcpy(outBuf+16, ct, ctLen);
	memcpy(hmac_ct, HMAC(EVP_sha256(), K->hmacKey, KLEN_SKE, outBuf, ctLen+16, NULL, NULL),32);
	printf("enc hmac_ct: %s\n", hmac_ct);
	memcpy(outBuf+16+ctLen, hmac_ct, 32);

	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	/* TODO: should return number of bytes written, which hopefully matches ske_getOutputLen(...). */

	return ctLen+48;

}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{

unsigned char * temp = malloc(48);
FILE * inFile = fopen(fnout,"r");
fread(temp,48,1,inFile);
printf("temp-: %s\n", temp);
fclose(inFile);

	int fd = open(fnin, O_RDONLY);
	//unsigned char* temp = malloc(200);

	unsigned char* map;
	int i;
	map = mmap(0, 0x1000,PROT_READ, MAP_SHARED,fd,0);
//printf("map: %s\n", map);
	size_t ctLen = strlen((const char*)map);
	unsigned char* ct = malloc(ctLen);
	size_t nBytes;

	nBytes = ske_encrypt(ct, map, ctLen, K, IV);
printf("ct: %s\n", ct);
	close(fd);
	fd = open(fnout,O_RDWR | O_CREAT, (mode_t)0600);
	lseek(fd,offset_out,SEEK_SET);
	write(fd,ct,nBytes);
	close(fd);

temp = malloc(48+nBytes);
inFile = fopen(fnout,"r");
fread(temp,48+nBytes,1,inFile);
printf("temp+: %s\n", temp);
fclose(inFile);

	/* TODO: write this.  Hint: mmap. */
	return nBytes;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	unsigned char* hmac_ct = malloc(32);
	unsigned char* hmac_check = malloc(32);
	unsigned char* IV = malloc(16);
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	//hmac which is derived from the IV and ct
	printf("HI2 %s\n", "");
	memcpy(hmac_ct, HMAC(EVP_sha256(), K->hmacKey, 32, inBuf, len-32, NULL, NULL),32);
	printf("HI3 %s\n", "");
	//hmac which is the last 32 bytes of the ciphertext
	memcpy(hmac_check, inBuf+(len-32),32);
	printf("hmac_ct %s\n", hmac_ct);
	printf("hmac_check %s\n", hmac_check);
	memcpy(IV, inBuf,16);
	size_t ctLen = len - 48;
	unsigned char* ct = malloc(ctLen);
	memcpy(ct, inBuf+16,ctLen);
	if(memcmp(hmac_ct, hmac_check,32)){
		printf("oh no2 %s\n", "");
		return -1;
	}
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	int nWritten;
	if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,ct,ctLen))
		ERR_print_errors_fp(stderr);
	size_t ptLen = nWritten;
	EVP_CIPHER_CTX_free(ctx);

	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	return ptLen;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{

	//int fd = open(fnin, O_RDONLY);
	//unsigned char* map;
	//int i;
	//map = mmap(0, 0x1000,PROT_READ, MAP_SHARED,fd,0);
	//printf("map: %s\n", map);
	//size_t ctLen = strlen((const char*)map);

	//map = malloc(ctLen);
	//close(fd);


FILE * inFile = fopen(fnin,"r");
fseek(inFile,0,SEEK_END);
size_t inSize = ftell(inFile);
rewind(inFile);
unsigned char* inBuf = malloc(inSize);
unsigned char* pt = malloc(inSize-offset_in);
fread(inBuf,inSize,1,inFile);
printf("inBuffalo: %s\n", inBuf);
fclose(inFile);
	//rewind(fin);
	//fread(map,1,ctLen,fin);

	size_t nBytes;
	//close(fin);
	//printf("mappa %s\n", map);
	nBytes = ske_decrypt(pt, inBuf, inSize, K);
//printf("pt %s\n", pt);
	int fd = open(fnout,O_RDWR | O_CREAT, (mode_t)0600);
	lseek(fd,offset_in,SEEK_SET);
	write(fd,pt,nBytes);
	//ftruncate(fd,nBytes);
	close(fd);


	/* TODO: write this. */
	return nBytes;
}
