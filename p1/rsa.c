#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */

        size_t keyBytes = keyBits/8;
        unsigned char* outBuf = malloc(keyBits);
        NEWZ(tmp1); NEWZ(tmp2); NEWZ(totient);
        randBytes(outBuf, keyBytes);
        BYTES2Z(tmp1, outBuf, keyBytes);
        while(ISPRIME(tmp1) < 1)
        {
            randBytes(outBuf, keyBytes);
            BYTES2Z(tmp1, outBuf, keyBytes);
        }
        mpz_set(K->p, tmp1);
        randBytes(outBuf, keyBytes);
        BYTES2Z(tmp2, outBuf, keyBytes);
        while((ISPRIME(tmp2) < 1) && (mpz_cmp(K->p, tmp2) != 0))
        {
            randBytes(outBuf, keyBytes);
            BYTES2Z(tmp2, outBuf, keyBytes);
        }
        mpz_set(K->q, tmp2);

        mpz_mul(K->n, K->p, K->q);
        mpz_sub_ui(tmp1, K->p, 1);
        mpz_sub_ui(tmp2, K->q, 1);
        mpz_mul(totient, tmp1, tmp2);

        randBytes(outBuf, keyBytes);
        BYTES2Z(K->e, outBuf, keyBytes);
        while((mpz_cmp_ui(tmp1, 1) != 0) || (mpz_cmp(K->e, totient) >= 0))
        {
            randBytes(outBuf, keyBytes);
            BYTES2Z(K->e, outBuf, keyBytes);
            mpz_gcd(tmp1, K->e, totient);
        }

        /*  randBytes(outBuf, keyBits);
        mpz_import(K->p, sizeof(outBuf), 1, sizeof(outBuf[0]), 0, 0, outBuf);
        >>>>>>> b5d6cd254cb1cfb92a8d0b3891950c8bd9a49a63
        while(ISPRIME(K->p) < 1)
        {
        randBytes(outBuf, keyBits);
        mpz_import(K->p, sizeof(outBuf), 1, sizeof(outBuf[0]), 0, 0, outBuf);
        }
        randBytes(outBuf, keyBits);
        mpz_import(K->q, sizeof(outBuf), 1, sizeof(outBuf[0]), 0, 0, outBuf);
        while((ISPRIME(K->q) < 1) && (mpz_cmp(K->p, K->q) != 0))
        {
        randBytes(outBuf, keyBits);
        mpz_import(K->q, sizeof(outBuf), 1, sizeof(outBuf[0]), 0, 0, outBuf);
        }

        mpz_mul(K->n, K->p, K->q);
        mpz_init(tmp1);
        mpz_init(tmp2);
        mpz_init(totient);
        mpz_sub_ui(tmp1, K->p, 1);
        mpz_sub_ui(tmp2, K->q, 1);
        mpz_mul(totient, tmp1, tmp2);

        randBytes(outBuf, keyBits);
        mpz_import(K->e, sizeof(outBuf), 1, sizeof(outBuf[0]), 0, 0, outBuf);
        while((mpz_cmp_ui(tmp1, 1) != 0) || (mpz_cmp(K->e, totient) >= 0))
        {
        randBytes(outBuf, keyBits);
        mpz_import(K->e, sizeof(outBuf), 1, sizeof(outBuf[0]), 0, 0, outBuf);
        mpz_gcd(tmp1, K->e, totient);
        }
        */

        mpz_invert(K->d, K->e, totient);

        /*
        mpz_gcd(tmp1, K->e, totient);
        gmp_printf("gcd(e,totient) = %Zd\n", tmp1);
        mpz_mul(tmp1, K->e, K->d);
        mpz_mod(tmp2, tmp1, totient);
        gmp_printf("K->p = %Zd\n", K->p);
        gmp_printf("K->q = %Zd\n", K->q);
        gmp_printf("K->n = %Zd\n", K->n);
        gmp_printf("K->e = %Zd\n", K->e);
        gmp_printf("K->d = %Zd\n", K->d);
        gmp_printf("tmp1 = %Zd\n", tmp1);
        gmp_printf("(e*d)%%totient = %Zd\n", tmp2);
        printf("rsa_numBytesN: %d\n\n", rsa_numBytesN(K));
        */

        mpz_clear(tmp1);
        mpz_clear(tmp2);
        mpz_clear(totient);

	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  Use BYTES2Z to get integers, and then
	 * Z2BYTES to write the output buffer. */
	NEWZ(text);
	NEWZ(cText);
//	printf("\nEncrypt function\n");
//	printf("text = %s\n", inBuf);
	BYTES2Z(text, inBuf, len);
	mpz_powm(cText, text, K->e, K->n);

        size_t size = mpz_sizeinbase(cText, 8);
        Z2BYTES(outBuf, size, cText);
//	printf("cText = %s\n\n", outBuf);
	mpz_clear(text);
	mpz_clear(cText);
	return size; /* TODO: return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  See remarks above. */
	NEWZ(text);
	NEWZ(cText);
//	printf("Decrypt function\n");
//	printf("cText = %s\n", inBuf);
	BYTES2Z(cText, inBuf, len);
	mpz_powm(text, cText, K->d, K->n);

	Z2BYTES(outBuf, len, text);
	//size_t size = mpz_sizeinbase(text, 8);
	////Z2BYTES(outBuf, size+1, text);
	////printf("text = %s\n\n", outBuf);
	//unsigned char* temp = malloc(size+1);
	//Z2BYTES(temp, size, text);
	//memcpy(outBuf, temp, size+1);
//	//printf("text = %s\n\n", outBuf);
	mpz_clear(text);
	mpz_clear(cText);
	//return size;
	return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
