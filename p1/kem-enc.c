/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>
//#include <sys/mman.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	//printf("fnIn: %s\n",fnIn);
	//printf("fnOut: %s\n",fnOut);
        //gmp_printf ("p %Zd\n", K->p);
        //gmp_printf ("q %Zd\n", K->q);
        //gmp_printf ("n %Zd\n", K->n);
        //printf("mpz_size: %d\n", mpz_size(K->n));
        //printf("limb size: %d\n", sizeof(mp_limb_t));
	FILE * inFile;
	FILE * outFile;
	inFile = fopen(fnIn,"r");
	outFile = fopen(fnOut,"w+");

	//fseek(inFile,0,SEEK_END);
	//size_t inSize = ftell(inFile)-1;
	//rewind(inFile);
	//unsigned char* inBuf = malloc(inSize);
	//fread(inBuf,1,inSize,inFile);

	//MY CODE
	//encrypt random stuff of size of rsa key K
	//size_t ctLen, mLen = rsa_numBytesN(K);
	//unsigned char* pt = malloc(mLen);
	//unsigned char* ct = malloc(mLen);
        //pt[mLen-1] = 0; /* avoid reduction mod n */
	//randBytes(pt,mLen-1);
        //ctLen = rsa_encrypt(ct,pt,mLen,&K);
        //printf("IM HERE!! \n\n\n");
        //// copied from rsa-test.c
        //printf("mLen: %d\n", mLen);
        //MYCODE

	//encrypt inBuf with symmetric key
	size_t rsaLen = rsa_numBytesN(K);
	unsigned char* x = malloc(rsaLen);
	//unsigned char* x = malloc(inSize);

	x[rsaLen-1] = 0;
	randBytes(x,rsaLen-1);
	//randBytes(x,inSize);

	//encapsulate SK using rsa and SHA256
	unsigned char* encaps = malloc(rsaLen);
	//unsigned char* encaps = malloc(rsaLen);
        printf("encapsulated:   %s\n", encaps);
        printf("old x:   %s\n", x);
        printf("rsaLen:   %zu\n", rsaLen);

	rsa_encrypt(encaps,x,rsaLen, K);
        printf("encapsulated:   %s\n", encaps);
        //memcpy(encaps,encapsulated,rsaLen);
        //printf("encaps: %s\n", encaps);
	unsigned char* outBuf = malloc(rsaLen+HASHLEN);
	unsigned char* hash = malloc(HASHLEN);
	SHA256_CTX sha256;
    	SHA256_Init(&sha256);
    	SHA256_Update(&sha256, x, HASHLEN);
    	SHA256_Final(hash, &sha256);

	memcpy(outBuf,encaps, rsaLen);
        printf("hash:   %s\n", hash);
	memcpy(outBuf+rsaLen,hash,HASHLEN);
	printf("outBuf: %s\n", outBuf);
	fwrite(outBuf,rsaLen+HASHLEN,1,outFile);
	fclose(outFile);

	printf("hasho:   %s\n", hash);

        unsigned char* temp = malloc(rsaLen+HASHLEN);
        inFile = fopen(fnOut,"r");
        fread(temp,rsaLen+HASHLEN,1,inFile);
        printf("temp: %s\n", temp);
        fclose(inFile);

	SKE_KEY SK;

	ske_keyGen(&SK,x,rsaLen);

	//size_t ctLen = ske_getOutputLen(inSize);
	//printf("ctLen: %zu\n", ctLen);
	//unsigned char* ct = malloc(ctLen);
	size_t offset = rsaLen+HASHLEN;
	printf("offset: %zu\n", offset);

	ske_encrypt_file(fnOut, fnIn, &SK, NULL, offset);
	//printf("HI %s\n", "");
	//printf("input: %s\n", "HI2");

	//printf("blank outBuf1:           %s\n", outBuf);
	//memcpy(outBuf, encaps, rsaLen);
	printf("encaps111:                  %s\n", encaps);
	//printf("outBuf w encaps:         %s\n", outBuf);
	//memcpy(outBuf+rsaLen, hash, 256/8);
	//printf("hash:                    %s\n", hash);
	//printf("outbuf w encaps+hash:    %s\n", outBuf);
	//memcpy(outBuf+rsaLen+(256/8), ct, ctLen);
	//printf("ct:                      %s\n", ct);
	//printf("outBuf w encaps+hash+ct: %s\n", outBuf);
	//fprintf(outFile, "%s", outBuf);

	//concatenate encapsulation and ciphertext
	// outBuf = encaps + hash + ct

temp = malloc(rsaLen+HASHLEN);;
inFile = fopen(fnOut,"r");
fread(temp,rsaLen+HASHLEN,1,inFile);
printf("temp3: %s\n", temp);
fclose(inFile);

	//fclose(inFile);
	//fclose(outFile);

	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */

	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	FILE * inFile;
	//FILE * outFile;
	inFile = fopen(fnIn,"r");
	//outFile = fopen(fnOut,"w");

	fseek(inFile,0,SEEK_END);
	size_t inSize = ftell(inFile);
	rewind(inFile);
	unsigned char* inBuf = malloc(inSize);
	fread(inBuf,1,inSize,inFile);
	printf("inSize %zu\n",inSize);
	printf("inBuf %s\n", inBuf);

	//get encaps and extract the sk
	size_t rsaLen = rsa_numBytesN(K);
	unsigned char* encaps = malloc(rsaLen);
	//fread(encaps,rsaLen+HASHLEN,1,inFile);
	memcpy(encaps,inBuf,rsaLen);
	//printf("encaps %s\n", encaps);

	unsigned char* sk = malloc(rsaLen);
printf("encaps2:   %s\n", encaps);
	memcpy(sk,encaps,rsaLen);
printf("encaps2:   %s\n", encaps);
	rsa_decrypt(sk,encaps,rsaLen, K);
printf("new x:   %s\n", sk);

	//check hash of sk with the rest of encaps
	unsigned char* hash = malloc(HASHLEN);
printf("hashb %s\n", hash);
	SHA256_CTX sha256;
    	SHA256_Init(&sha256);
    	SHA256_Update(&sha256, sk, HASHLEN);
    	SHA256_Final(hash, &sha256);
printf("hasha %s\n", hash);

	unsigned char* sk_hash = malloc(HASHLEN);
	memcpy(sk_hash, inBuf+rsaLen, HASHLEN);
	printf("hash1 %s\n", hash);
	printf("hash2 %s\n", sk_hash);
	if(memcmp(hash,sk_hash,HASHLEN)){
		printf("oh no %s\n", "");
		//return -1;
	}
	size_t offset = rsaLen+HASHLEN;
	SKE_KEY SK;
	ske_keyGen(&SK, sk,rsaLen);
	ske_decrypt_file(fnOut,fnIn,&SK,offset);

	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	return 0;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	FILE* file;
	FILE* keyfile;
	RSA_KEY K;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);

				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */
	switch (mode) {
		case ENC:
			keyfile = fopen(fnKey,"r");
			rsa_readPublic(keyfile,&K);
			kem_encrypt(fnOut,fnIn,&K);
                        fclose(keyfile);
                        rsa_shredKey(&K);
			break;
		case DEC:
			keyfile = fopen(fnKey,"r");
			rsa_readPrivate(keyfile,&K);
			kem_decrypt(fnOut, fnIn, &K);
			fclose(keyfile);
			rsa_shredKey(&K);
                        break;
		case GEN:
			file = fopen(fnOut,"w");
			rsa_keyGen(nBits,&K);

                        //printf("HIIIIIIIIIIIIIIII\n\n\n\n");
                        //gmp_printf ("p %Zd\n", K.p);
                        //gmp_printf ("q %Zd\n", K.q);
                        //gmp_printf ("n %Zd\n", K.n);
                        //printf("mpz_size: %d\n", mpz_size(K.n));
                        //printf("limb size: %d\n", sizeof(mp_limb_t));

			rsa_writePrivate(file,&K);
			fclose(file);
			file = fopen(strcat(fnOut,".pub"),"w");
			rsa_writePublic(file,&K);
			fclose(file);
                        rsa_shredKey(&K);
                        break;
		default:
			return 1;
	}
	return 0;
}
